# Guia Python

1. **Definir diferentes variables de tipo int, float y string y mostrar sus valores en la terminal interactiva y utilizando la función print.** 

![Guia Python - Ejercicio 1](guiaPython_1.jpg)


2. **¿Qué acciones realizan las funciones type() y dir()? (Ver ayuda con el comando type? y dir?)**

![Guia Python - Ejercicio 2](guiaPython_2.jpg)

La función type() en Python se utiliza para obtener el tipo de un objeto. Por ejemplo, si tienes una variable "int_a" y quieres saber qué tipo de datos almacena, puedes usar type(int_a) y te devolverá el tipo de datos, como int, str, list, dict, etc.

La función dir() en Python se utiliza para obtener una lista de nombres de atributos válidos para un objeto. Si llamas a dir() sin argumentos, te dará una lista de nombres en el espacio de nombres actual. Pero si le pasas un objeto como argumento, te dará una lista de atributos y métodos disponibles para ese objeto.


3. **¿Qué tipo de dato devuelve al ejecutar la sentencia?**
```
dir(list())
```
**¿Qué significado tiene su contenido?**

La sentencia dir(list()) devuelve una lista de los nombres de atributos válidos para un objeto de tipo list. Al utilizar la función dir() en Python, se pueden listar los nombres de los atributos (métodos y variables) definidos en un objeto.

El contenido de esta lista incluye todos los métodos y atributos accesibles para los objetos de tipo list. Esto abarca tanto los métodos y atributos propios de las listas, como aquellos heredados de las clases base de las que las listas derivan.

Algunos ejemplos de los métodos y atributos que podrían encontrarse en la salida de dir(list()) son append(), clear(), count(), extend(), index(), insert(), pop(), remove(), reverse(), sort(), y métodos especiales como __getitem__(), __len__(), entre otros.

![Guia Python - Ejercicio 3](guiaPython_3.jpg)

Métodos y atributos propios de la clase list: Estos son los métodos y atributos que están definidos específicamente para objetos de tipo list. Por ejemplo, métodos como append(), clear(), count(), extend(), index(), insert(), pop(), remove(), reverse(), sort(), entre otros.

Métodos especiales (__*__): También puedes encontrar métodos especiales, también conocidos como métodos mágicos, que Python utiliza internamente para realizar operaciones específicas en objetos de tipo list. Por ejemplo, __getitem__() para acceder a elementos mediante índices, __len__() para obtener la longitud de la lista, entre otros.

Otros métodos y atributos de la clase base: La lista también puede tener métodos y atributos heredados de otras clases base o clases mixinas.

4. ** Buscar información sobre las 3 maneras de formatear una cadena: i)"printf-stype" con %s, ii) usando .format() y iii) con "f-string".**


_Printf-style con %s:_
Esta es una forma más antigua de formatear cadenas en Python, similar a la sintaxis utilizada en el lenguaje > de programación C. Se basa en el operador % y se utiliza de la siguiente manera:

```
nombre = "Leo"
cadena_formateada = "Mi nombre es %s" % (nombre)
print(cadena_formateada)
```
En este ejemplo, %s se utiliza como marcador de posición para el valores de nombre.

_Método .format():_
Este método fue introducido en Python 2.7 y proporciona una forma más flexible y legible de formatear cadenas. Se utiliza de la siguiente manera:

```
nombre = "Leo"
cadena_formateada = "Mi nombre es {}.".format(nombre)
print(cadena_formateada)
```

Aquí, {} se utiliza como marcador de posición para los valores de nombre.

_F-strings:_
También conocidas como "formatted string literals", las f-strings son una característica introducida en Python 3.6 que ofrece una forma muy intuitiva y poderosa de formatear cadenas. Se utilizan de la siguiente manera:

```
nombre = "Leo"
cadena_formateada = f"Mi nombre es {nombre}."
print(cadena_formateada)
```

Aquí, {nombre} se utiliza como marcadores de posición y las variables se insertan directamente dentro de la cadena.


5. **Utilizar el módulo matemático (math) para imprimir el valor de la constante \pi. Mostrar su valor con 3, 6 y 10 decimales utilizando el formateo de cadenas .format() y "f-string".**


_Utilizando .format()_
```
import math

pi = math.pi

# Con 3 decimales
print("Valor de pi con 3 decimales: {:.3f}".format(pi))

# Con 6 decimales
print("Valor de pi con 6 decimales: {:.6f}".format(pi))

# Con 10 decimales
print("Valor de pi con 10 decimales: {:.10f}".format(pi))
```

_Utilizando f-string_
```
import math

pi = math.pi

# Con 3 decimales
print(f"Valor de pi con 3 decimales: {pi:.3f}")

# Con 6 decimales
print(f"Valor de pi con 6 decimales: {pi:.6f}")

# Con 10 decimales
print(f"Valor de pi con 10 decimales: {pi:.10f}")
```
![Guia Python - Ejercicio 5 - f-string](guiaPython_5.jpg)

6. ¿Cuáles son los métodos del tipo de datos str()?

![Guia Python - Ejercicio 6 - f-string](guiaPython_6.jpg)

7. **¿Qué hacen los siguientes métodos de una lista: append(), clear(), count(), index(). ¿Cómo se puede agregar y eliminar un elemento del medio?**


append(): Este método se utiliza para agregar un elemento al final de la lista.


```
lista = [1, 2, 3]
lista.append(4)
```



clear(): Este método se utiliza para eliminar todos los elementos de la lista, dejándola vacía.

```
lista = [1, 2, 3]
lista.clear()
# lista ahora es []
```


count(): Este método se utiliza para contar cuántas veces aparece un elemento en la lista. 

```
lista = [1, 2, 2, 3, 3, 3]
cantidad_de_tres = lista.count(3)
# cantidad_de_tres ahora es 3
```


index(): Este método se utiliza para encontrar la primera ocurrencia de un elemento en la lista y devuelve su índice. 

```
lista = ['a', 'b', 'c', 'b', 'a']
indice_de_b = lista.index('b')
# indice_de_b ahora es 1
```


Para agregar un elemento en una posición específica, puedes usar el método insert().

```
lista = [1, 2, 3, 4]
lista.insert(2, 'nuevo')
# lista ahora es [1, 2, 'nuevo', 3, 4]
```


Para eliminar un elemento de una posición específica, puedes usar el operador del o el método pop().

```
lista = [1, 2, 3, 4]
del lista[2]  # Elimina el elemento en el índice 2
# lista ahora es [1, 2, 4]

elemento_eliminado = lista.pop(2)  # Elimina y devuelve el elemento en el índice 2
# elemento_eliminado es 4
# lista ahora es [1, 2]
```

8. **¿Qué hace el slice al aplicarlo sobre una lista my_listcomo my_list[:]?**

Cuando aplicas el slice my_list[:] sobre una lista my_list, estás creando una copia superficial (shallow copy) de la lista original. Esto significa que estás obteniendo una nueva lista que contiene los mismos elementos que la lista original, pero no son los mismos objetos.

El uso de my_list[:] es útil cuando deseas manipular una lista sin modificar la original. Por ejemplo, puedes usarlo para crear una copia de seguridad antes de realizar operaciones que alteren la lista original.

9. **¿Qué devuelve la ejecución de dir(tuple()) y qué contiene?**

La función dir() en Python como ya vimos devuelve una lista de los nombres de atributos válidos (como cadenas) para el objeto especificado. Cuando llamas dir(tuple()), estás obteniendo una lista de los nombres de atributos válidos para un objeto de tipo tuple.

![Guia Python - Ejercicio 9 - f-string](guiaPython_9.jpg)


10. **Crear una tupla tup = (1,2,3,4). ¿Qué se obtiene al hacer tup[0] = 5?**

En Python, las tuplas son estructuras de datos inmutables, lo que significa que una vez que se crean, no se pueden modificar. Intentar modificar un elemento de una tupla generará un error.

Cuando se intenta hacer tup[0] = 5 con la tupla tup = (1, 2, 3, 4), se recibe un error de tipo SintaxError, indicando que no se puede realizar la asignación porque las tuplas no admiten la asignación de elementos individuales. Esto es así porque las tuplas son inmutables en Python.

![Guia Python - Ejercicio 10 - f-string](guiaPython_10.jpg)

Si se necesita realizar cambios en una estructura de datos similar a una tupla, se debe considerar el uso de una lista en su lugar, ya que las listas son mutables y permiten este tipo de operaciones.

11. **¿Cuántos elementos tiene la siguiente tupla: tup_b = (10,)? ¿Qué sucede si se omite la coma?**

La tupla tup_b = (10,) tiene un solo elemento. Aunque la coma al final puede parecer redundante, es importante entender que en Python, una coma final después del último elemento en una tupla es lo que realmente indica que estás creando una tupla de un solo elemento. Sin la coma final, Python no interpretará la expresión como una tupla de un solo elemento, sino simplemente como un valor entre paréntesis.
